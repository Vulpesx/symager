use std::{io, path::Path};
use thiserror::Error;

#[derive(Debug, Clone)]
pub struct SymEntry {
    pub name: String,
    pub dest: String,
}

impl SymEntry {
    pub fn new(name: &str, dest: &str) -> Self {
        Self {
            name: name.trim_start_matches('@').to_string(),
            dest: dest.trim_end_matches('/').to_string(),
        }
    }

    pub fn link_name(&self) -> String {
        format!("@{}.{}", self.name, self.dest)
    }

    pub fn link<T: AsRef<Path>>(&self, dir: T, force: bool) -> Result<(), SymError> {
        let p = dir.as_ref().join(self.link_name());
        let d = dir.as_ref().join(&self.dest);
        if d.exists() {
            if !d.is_symlink() && !force {
                return Err(SymError::NotLink(d.to_string_lossy().to_string()));
            }
            std::fs::remove_file(&d)?;
            while d.exists() {}
        }
        std::os::unix::fs::symlink(p, d)?;
        Ok(())
    }
}

impl TryFrom<&str> for SymEntry {
    type Error = SymError;

    fn try_from(s: &str) -> std::result::Result<Self, Self::Error> {
        if !s.starts_with('@') {
            return Err(SymError::NoLink);
        }
        let Some((name, dest)) = s.split_once('.') else {
            return Err(SymError::NoDest);
        };
        Ok(Self::new(name, dest))
    }
}

#[derive(Error, Debug)]
pub enum SymError {
    #[error("file system error: {0}")]
    Fs(#[from] io::Error),
    #[error("No link signifier")]
    NoLink,
    #[error("No link destination")]
    NoDest,
    #[error("Not a directory")]
    NotDir,
    #[error("{0} exists and is not a link")]
    NotLink(String),
}

pub fn get_entries<T: AsRef<Path>>(dir: T) -> Result<Vec<SymEntry>, SymError> {
    let dir = dir.as_ref();
    let mut entries = vec![];
    if !dir.is_dir() {
        return Err(SymError::NotDir);
    }
    for e in dir.read_dir().expect("how???") {
        let Ok(e) = e else {
            continue;
        };
        let p = e.path();
        let n = p.file_name().unwrap().to_str().unwrap();
        if !n.starts_with('@') {
            continue;
        }
        let Some((name, dest)) = n.split_once('.') else {
            continue;
        };
        entries.push(SymEntry::new(name, dest));
    }

    Ok(entries)
}
