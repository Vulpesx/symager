use std::path::PathBuf;

use clap::{Parser, Subcommand};

/// symlink manager
///
/// easily manage and change multiple versions of a file or directory
/// using symager links, which are just files or directories
/// with funky names: "@[SYMAGER_NAME].[LINK_NAME]"
#[derive(Parser)]
pub struct Cli {
    /// working directory
    #[arg(short, long, default_value = ".")]
    pub dir: PathBuf,

    #[command(subcommand)]
    pub cmd: Command,
}

#[derive(Subcommand)]
pub enum Command {
    /// create a new symager link
    New {
        /// pre-existing file or dir to link to
        path: PathBuf,
        /// name of symager link
        name: String,
    },

    /// use a symager link
    Link {
        /// symager link name or full file name
        name: Option<String>,

        /// force link, WILL DELETE FILES IF NECISSARY
        #[arg(short, long)]
        force: bool,
    },

    /// remove a symager link
    Rm {
        name: Option<String>,

        /// delete link instead of reverting it
        #[arg(short, long)]
        delete: bool,
    },
}
