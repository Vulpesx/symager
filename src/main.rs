use anyhow::{bail, Result};
use clap::Parser;
use cli::{Cli, Command};
use sym::SymEntry;

mod cli;
mod sym;

fn main() -> Result<()> {
    let cli = Cli::parse();
    match cli.cmd {
        Command::New { path, name } => {
            let dest = path
                .file_name()
                .expect("couldnt get file name [1]")
                .to_str()
                .expect("couldnt get file name [2]");

            let f = SymEntry::new(&name, dest);
            std::fs::rename(path, f.link_name())?;
        }
        Command::Link { name, force } => {
            if let Some(n) = name {
                if let Ok(s) = SymEntry::try_from(n.as_str()) {
                    s.link(&cli.dir, force)?;
                } else {
                    let entries: Vec<_> = sym::get_entries(&cli.dir)?
                        .into_iter()
                        .filter(|e| e.name == n)
                        .collect();

                    if entries.len() == 1 {
                        entries[0].link(&cli.dir, force)?;
                    } else {
                        for (i, s) in entries.iter().enumerate() {
                            if s.name == n {
                                println!("[{i}] {n} <- {}", s.dest);
                            }
                        }
                        println!("Multiple links found; choose [0..]");
                        let mut buf = String::new();
                        std::io::stdin().read_line(&mut buf)?;
                        let i: usize = buf.trim().parse()?;
                        if i >= entries.len() {
                            bail!("index out of range");
                        }
                        entries[i].link(&cli.dir, force)?;
                    }
                }
            } else {
                let entries = sym::get_entries(&cli.dir)?;
                if entries.len() == 1 {
                    entries[0].link(&cli.dir, force)?;
                } else if entries.len() > 1 {
                    for (i, s) in entries.iter().enumerate() {
                        println!("[{i}] {} <- {}", s.name, s.dest);
                    }
                    println!("Multiple links found; choose [0..]");
                    let mut buf = String::new();
                    std::io::stdin().read_line(&mut buf)?;
                    let i: usize = buf.trim().parse()?;
                    if i >= entries.len() {
                        bail!("index out of range");
                    }
                    entries[i].link(&cli.dir, force)?;
                }
            }
        }
        Command::Rm { name, delete } => todo!(),
    }
    Ok(())
}
